FROM ubuntu:18.04

RUN apt-get update && apt-get install -y

RUN apt-get install -y curl

RUN curl -L "https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

RUN chmod +x /usr/local/bin/docker-compose

RUN apt-get install -y openjdk-11-jdk

RUN apt-get install -y maven

